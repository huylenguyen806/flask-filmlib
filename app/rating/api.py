from flask import request, jsonify, abort
from flask_restful import Resource
from app.models import Rating, db


class RatingAPI(Resource):
    def get(self, id=None):
        if not id:
            ratings = Rating.query.all()
            res = {}
            for rat in ratings:
                res[rat.id] = {
                    'rev_id': rat.rev_id,
                    'rev_stars': rat.rev_stars,
                    'rev_comment': rat.rev_comment
                }
        else:
            rat = Rating.query.get(id)
            if not rat:
                abort(404)
            res = {
                'rev_id': rat.rev_id,
                'rev_stars': rat.rev_stars,
                'rev_comment': rat.rev_comment
            }
        return jsonify(res)

    def post(self):
        rev_id = request.form.get('rev_id')
        rev_stars = request.form.get('rev_stars')
        rev_comment = request.form.get('rev_comment')
        new_rat = Rating(rev_stars, rev_comment, rev_id)

        db.session.add(new_rat)
        db.session.commit()

        return jsonify({
            'rev_id':  new_rat.rev_id,
            'rev_stars': new_rat.rev_stars,
            'rev_comment': new_rat.rev_comment
        })

    def put(self, id):
        rat = Rating.query.get(id)
        rev_stars = request.form.get('rev_stars')
        rev_comment = request.form.get('rev_comment')
        rat.rev_comment = rev_comment
        rat.rev_stars = rev_stars

        db.session.commit()

        return jsonify({
            'rev_id': rat.rev_id,
            'rev_stars': rat.rev_stars,
            'rev_comment': rat.rev_comment
        })

    def delete(self, id):
        rat = Rating.query.get(id)

        db.session.delete(rat)
        db.session.commit()

        return jsonify({
            "message": "delete rating {} successfully".format(rat.rev_id)
        })
