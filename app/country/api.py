from flask import request, jsonify, abort
from flask_restful import Resource
from app.models import Country, db


class CountryAPI(Resource):
    def get(self, id=None):
        if not id:
            countries = Country.query.all()
            res = {}
            for c in countries:
                res[c.id] = {
                    'name': c.name
                }
        else:
            c = Country.query.get(id)
            if not c:
                abort(404)
            res = {
                'name': c.name
            }

        return jsonify(res)

    def post(self):
        name = request.form.get('name')
        new_c = Country(name)

        db.session.add(new_c)
        db.session.commit()

        return jsonify({
            'name': new_c.name
        })

    def put(self, id):
        c = Country.query.get(id)
        c.name = request.form.get('name')

        db.session.commit()

        return jsonify({
            'name': c.name
        })

    def delete(self, id):
        c = Country.query.get(id)

        db.session.delete(c)
        db.session.commit()

        return jsonify({
            "message": "delete country {} successfully".format(c.name)
        })
