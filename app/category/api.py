from flask import request, jsonify, abort
from flask_restful import Resource
from app.models import Category, db


class CategoryAPI(Resource):
    def get(self, id=None):
        if not id:
            categories = Category.query.all()
            res = {}
            for ca in categories:
                res[ca.id] = {
                    'name': ca.name
                }
        else:
            ca = Category.query.get(id)
            if not ca:
                abort(404)
            res = {
                'name': ca.name
            }
        return jsonify(res)

    def post(self):
        name = request.form.get('name')

        new_ca = Category(name)

        db.session.add(new_ca)
        db.session.commit()

        return jsonify({
            'name': new_ca.name
        })

    def put(self, id):
        ca = Category.query.get(id)
        name = request.form.get(id)
        ca.name = name

        db.session.commit()

        return jsonify({
            'name': ca.name
        })

    def delete(self, id):
        ca = Category.query.get(id)

        db.session.delete(ca)
        db.session.commit()

        return jsonify({
            "message": "delete category {} successfully".format(ca.name)
        })
